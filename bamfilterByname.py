#!/usr/bin/env python
import pysam
import sys
infile = pysam.AlignmentFile(sys.argv[1], "rb")
outfile = pysam.AlignmentFile("{:s}.bam".format(sys.argv[2]), "wb", template=infile)

query = set(open(sys.argv[2]).read().strip().split("\n"))
# print query
for read in infile.fetch():
	if read.qname in query:
		# if not read.is_supplementary:
			outfile.write(read)

infile.close()
outfile.close()
