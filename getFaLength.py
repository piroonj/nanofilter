#!/usr/bin/env python
import argparse,sys,re,os
import pysam
import pandas as pd
from Bio import SeqIO, bgzf
from Bio.Alphabet import generic_dna
import gzip
from numpy import mean, array, amin

## version 2.5.1
# introduce filter by names
# ## v2.5 add converse sequence to reverse-complement for direct cDNA reads
version="2.5.1"
parser = argparse.ArgumentParser(description='get sequence length from fasta file')
parser.add_argument('-i','--infile', dest='infile', metavar='<fasta/fastq>',
                    type=str, default="", help="input file (default: stdin)")
parser.add_argument('-q','--in-fastq', action='store_true', dest='infastq',
                    help="input in fastq (default: false)")
parser.add_argument('-out','--out-datatype', dest='out', type=str, default='raw',
                    choices=['raw', 'fastq', 'fasta'],
                    help="defind out data type (default: raw)")
parser.add_argument('-in','--in-datatype', dest='intype', type=str, default='raw',
                    choices=['raw', 'fastq', 'fasta'],
                    help="defind in data type (default: raw)")
parser.add_argument('-t','--task', dest='task', type=str, default="count",
					choices=['count', 'filter', 'nt_count', 'assembly'],
                    help="select task (default: count)")
parser.add_argument('-excl','--excl_names', dest='excl_names', type=str, default="",
                    metavar='<str>', help="exclude by id names list")
parser.add_argument('-incl','--incl_names', dest='incl_names', type=str, default="",
                    metavar='<str>', help="include by id names list")
parser.add_argument('-min','--min-lenght', dest='min', type=int, default=0,
                    metavar='<int>', help="minimum lenght (default: 0)")
parser.add_argument('-max','--max-lenght', dest='max', type=int, default=1000000000000,
                    metavar='<int>', help="maximum lenght (default: 1000000000000)")
parser.add_argument('-d','--description', action='store_true', dest='description',
                    help="add descripiton (default: false)")
parser.add_argument('-alb','--albacore_summary', dest='albacore_summary', type=str,
                    default="", metavar='<str>', help="filter by albacore sequencing_summary")
parser.add_argument('--qscore', dest='qscore', type=int, default=0, 
                    metavar='<int>', help="set cut-off for Albacore quality score")
parser.add_argument('-rna','--rna2dna', action='store_true', dest='rna2dna',
                    help="convert RNA to DNA sequence (default: false)")
parser.add_argument('--dna2rna', action='store_true', dest='dna2rna',
                    help="convert RNA to DNA sequence (default: false)")
parser.add_argument('-cdna','--revcomp', action='store_true', dest='revcomp',
                    help="reverse-complement cDNA reads (default: false)")
parser.add_argument('-u','--unique', action='store_true', dest='uniq',
                    help="make unique id (default: false)")
parser.add_argument('-rpf','--read-per-file', dest='readPerFile', type=int, default=0,
                    metavar='<int>', help="number of reads per file. 0=no_limit (default: 0)")
parser.add_argument('--rlimit','--reads-limit', dest='readLimit', type=int, default=0,
                    metavar='<int>', help="number of reads limit. 0=no_limit (default: 0)")
parser.add_argument('-o','--out-file', dest='outf', type=str, default="",
                    metavar='<str>', help="defind prefix for output")
parser.add_argument('-header','--header', action='store_true', dest='header',
                    help="print header (default: false)")
parser.add_argument('-v','--version', action='store_true', dest='version',
                    help="")
args = parser.parse_args()
# print args

if args.version:
	exit("{:s} v{:s}".format(sys.argv[0],version))

if args.outf != "" and args.readPerFile == 0:
	if args.task in ["count","nt_count"]:
		ext=".txt"
		outf = open("{:s}{:s}".format(args.outf, ext), "w")
	elif args.task in ["filter"]:
		(pathFile, ext) = os.path.splitext(args.infile)
		outf = open("{:s}{:s}".format(args.outf, ext), "w")

def report(result, prefix=""):
	if prefix == "":
		print(result)
	else:
		global outf
		outf.write("{:s}\n".format(result))

if args.infile == "stdin":
    infile = sys.stdin
elif re.search('.gz$',args.infile):
    infile = gzip.open(args.infile,'rt')
elif re.search('.bgz$',args.infile):
    infile = gzip.open(args.infile,'rt')
else:
    infile = open(args.infile,"r")

if args.excl_names != "":
    if re.search("\.bam$", args.excl_names):
        samfile = pysam.AlignmentFile(args.excl_names, "rb")
        excl_names = set([])
        for l in samfile:
            excl_names.add(l.query_name)
        samfile.close()
    else:
        infile_names = open(args.excl_names)
        excl_names = set(infile_names.read().strip().split("\n"))
        infile_names.close()
if args.incl_names != "":
    if re.search("\.bam$", args.incl_names):
        samfile = pysam.AlignmentFile(args.incl_names, "rb")
        incl_names = set([])
        for l in samfile:
            incl_names.add(l.query_name)
        samfile.close()
    else:
        infile_names = open(args.incl_names)
        incl_names = set(infile_names.read().strip().split("\n"))
        infile_names.close()

if args.header:
    if args.task == "count":
        if args.description:
            report("seq_id\tdescription\tseq_length", args.outf)
        else:
            report("seq_id\tseq_length", args.outf)
    elif args.task == "nt_count":
        if args.description:
            report("seq_id\tdescription\tseq_length\tA\tT\tC\tG\tU\tN\tothers", args.outf)
        else:
            report("seq_id\tseq_length\tA\tT\tC\tG\tU\tN\tothers", args.outf)

if re.search("(\.fq|\.fastq)", args.infile):
    indataType = "fastq"
elif re.search("(\.fa|\.fasta)", args.infile):
    indataType = "fasta"
elif args.infastq:
    indataType = "fastq"
else:
    if args.intype == 'fastq':
        indataType = "fastq"
    elif args.intype == 'fasta':
        indataType = "fasta"
    else:
        exit("please provide fasta/fastq as input file")
# indataType = "fasta"
# if args.infastq:
#     indataType = "fastq"

def processSummary(summaryfile, readtype="1D"):
    '''take from nanoget.py 
    https://github.com/wdecoster/nanoget/blob/master/nanoget/nanoget.py
    Extracting information from an albacore summary file.
    Only reads which have a >0 length are returned.
    The fields below may or may not exist, depending on the type of sequencing performed.
    Fields 1-14 are for 1D sequencing.
    Fields 1-23 for 2D sequencing.
     1  filename
     2  read_id
     3  run_id
     4  channel
     5  start_time
     6  duration
     7  num_events
     8  template_start
     9  num_events_template
    10  template_duration
    11  num_called_template
    12  sequence_length_template
    13  mean_qscore_template
    14  strand_score_template
    15  complement_start
    16  num_events_complement
    17  complement_duration
    18  num_called_complement
    19  sequence_length_complement
    20  mean_qscore_complement
    21  strand_score_complement
    22  sequence_length_2d
    23  mean_qscore_2d
    '''
    if readtype == "1D":
        cols = ["read_id", "run_id", "channel", "start_time", "sequence_length_template", "mean_qscore_template"]
    elif readtype == "2D":
        cols = ["read_id", "run_id", "channel", "start_time",  "sequence_length_2d", "mean_qscore_2d"]
    try:
        datadf = pd.read_csv(
            filepath_or_buffer=summaryfile,
            sep="\t",
            usecols=cols,
            )
    except ValueError:
        sys.exit("ERROR: did not find expected columns in summary file:\n {}".format(', '.join(cols)))
    datadf.columns = ["readIDs", "runIDs", "channelIDs", "time", "lengths", "quals"]
    a_time_stamps = array(datadf["time"], dtype='datetime64[s]')
    datadf["start_time"] = a_time_stamps - amin(a_time_stamps)
    return datadf[datadf["lengths"] != 0]

if args.albacore_summary:
    df = processSummary(args.albacore_summary, readtype="1D")
    fil = set(df.loc[lambda df: df.quals >= args.qscore, 'readIDs'])

readPerFile=0
check_uniq = set()
numReads=0
if args.infile != "":
    if args.task in ["filter"]:
        (pathFile, ext) = os.path.splitext(args.infile)
        outf = open("{:s}.{:d}{:s}".format(args.outf, readPerFile, ext),"w")
        
    for record in SeqIO.parse(infile, indataType):
        if args.min <= len(record.seq) and args.max >= len(record.seq):
            if args.uniq:
                if record.id in check_uniq:
                    continue
                else: 
                    check_uniq.add(record.id)
            if args.excl_names != "":
                if record.id in excl_names:
                    continue
            if args.incl_names != "":
                if record.id not in incl_names:
                    continue
            if args.albacore_summary:
                if record.id.split("_")[0] not in fil:
                    continue
            if len(record.letter_annotations) > 0 and mean(record.letter_annotations["phred_quality"]) < args.qscore:
                continue

            if args.readPerFile > 0 and numReads % args.readPerFile == 0:
                if args.task in ["filter"]:
                    ##ext=".txt"
                    outf.close()
                    (pathFile, ext) = os.path.splitext(args.infile)
                    outf = open("{:s}.{:d}{:s}".format(args.outf, readPerFile, ext),"w")
                readPerFile+=1
            numReads+=1
            if args.task == "count":
                meanQscore = ""
                if indataType == "fastq":
                    meanQscore = "\t{:.2f}".format(mean(record.letter_annotations["phred_quality"]))
                if args.description:
                    report("{:s}\t{:s}\t{:d}{}".format(record.id, record.description, len(record.seq), meanQscore), args.outf)
                else:
                    report("{:s}\t{:d}{}".format(record.id, len(record.seq),meanQscore), args.outf)
            elif args.task == "filter":
                if args.rna2dna and args.revcomp:
                    exit("choose either --rna2dna or --revcomp")
                elif args.dna2rna:
                    record.seq = record.seq.transcribe()
                    record.description = record.description+" transcribe=Y"
                elif args.rna2dna:
                    record.seq = record.seq.back_transcribe()
                    record.description = record.description+" back_transcribe=Y"
                elif args.revcomp:
                    record_tmp = record.reverse_complement()
                    record_tmp.id = record.id
                    record_tmp.description = record.description + " reverse_complement=Y"
                    record = record_tmp
                    
                if args.out == 'raw':
                    report(record.format(indataType).strip(), args.outf)
                elif args.out == 'fasta':
                    report(record.format('fasta').strip(), args.outf)
                elif args.out == 'fastq':
                    report(record.format('fastq').strip(), args.outf)

                # if args.description:
                #     print ">%s\n%s"%(record.description, record.seq)
                # else:
                #     print ">%s\n%s"%(record.id, record.seq)
            elif args.task == "nt_count":
                a = record.seq.upper().count("A")
                t = record.seq.upper().count("T")
                c = record.seq.upper().count("C")
                g = record.seq.upper().count("G")
                u = record.seq.upper().count("U")
                n = record.seq.upper().count("N")
                import math
                o = len(record.seq) - math.fsum([a, t, c, g, u, n])
                dist = [a, t, c, g, u, n, o]
                if args.description:
                    report("%s\t%s\t%s\t%s"%(record.id, record.description, len(record.seq), "\t".join(map(str,dist))), args.outf)
                else:
                    report("%s\t%s\t%s"%(record.id, len(record.seq), "\t".join(map(str,dist))), args.outf)
            elif args.task == "assembly":
                a = record.seq.upper().count("A")
                t = record.seq.upper().count("T")
                c = record.seq.upper().count("C")
                g = record.seq.upper().count("G")
                u = record.seq.upper().count("U")
                n = record.seq.upper().count("N")
                import math
                o = len(record.seq) - math.fsum([a, t, c, g, u, n])
                gc = "{:.2f}".format(float(g+c)/len(record.seq)*100)
                if args.description:
                    report("%s\t%s\t%s\t%s"%(record.id, record.description, len(record.seq), gc), args.outf)
                else:
                    report("%s\t%s\t%s"%(record.id, len(record.seq), gc), args.outf)

if args.outf != "":
	outf.close()

