#!/usr/bin/env python
import pysam, re, sys
import argparse
from pandas import DataFrame as df
getNum = re.compile(r"^(\d+) ")
header = ['total','secondary','supplementary','duplicates','mapped','paired','read1','read2','properly_paired','itself_n_mate','singletons','diff_chr','diff_chr_mapq_gt_5']

def bamLongReadsMappedStats(infile):
    oStats = pysam.flagstat(infile)
    counts = [ int(getNum.search(x).group(1)) for x in oStats.strip().split("\n")]
    tab = dict(zip(header, counts))
    total = tab['total'] - tab['secondary'] - tab['supplementary']
    mapped = tab['mapped'] - tab['secondary'] - tab['supplementary']
    unmapped = total - mapped
    out = [infile, total, mapped, "{:.2f}".format(mapped/float(total)*100), unmapped, "{:.2f}".format(unmapped/float(total)*100)]
    return out

def get_args():
    version="1.0.0"
    parser = argparse.ArgumentParser(description='basic bam stats')
    parser.add_argument("--ibam", "-i",
                        help="Bam file(s)",
                        nargs='+', default=[],
                        metavar="file(s)")
    parser.add_argument('--output', '-o', type=str,
                        metavar="outfile", default="stdout")
    return parser

if __name__ == '__main__':
    parser = get_args()
    args = parser.parse_args()
    if len(args.ibam) == 0:
        print("no input file(s)\n")
        exit(parser.print_help())
    header_out = ["Filename","Total","Mapped","Percent_mapped","Unmapped","Percent_unmapped"]
    out = []
    for infile in args.ibam:
        out.append(bamLongReadsMappedStats(infile))
    out_df = df(out, columns=header_out)
    if args.output == "stdout":
        out_df.to_csv(sys.stdout, sep="\t", index=None)
    else:
        out_df.to_csv(args.output, sep="\t", index=None)


    